" Основные настройки
source ~/.vim/config/settings.vim

" Подключение плагинов
source ~/.vim/config/plugins_list.vim

" Установка темы (пришлось вытащить сюда)
colorscheme catppuccin_mocha

" Комбинации клавиш
source ~/.vim/config/mapping-en.vim
source ~/.vim/config/mapping-ru.vim

