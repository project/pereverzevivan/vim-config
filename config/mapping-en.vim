function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

function! CloseAllTeminals()
  execute "FloatermKill!"
endfunction

function! CloseVim()
  call CloseAllTeminals()
  execute "q"
endfunction

function! CloseVimWithoutSave()
  call CloseAllTeminals()
  execute "q!"
endfunction


" Определяем главную клавишу
let mapleader = " "

" Добавляем возможность использовать русскую раскладку для навигации и прочего
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz


" Основные
nnoremap <silent> <leader>w :w<cr>
nnoremap <silent> <c-q> :call CloseVim()<cr>
nnoremap <silent> <c-Q> :call CloseVimWithoutSave()<cr>
nnoremap <silent> <leader>u ~
nnoremap <silent> <c-z> u
nnoremap <silent> <c-s> :w<cr>
inoremap <silent> jj <esc>
nnoremap <silent> <leader>re :so %<cr>
nnoremap <silent> <leader>cr :echo expand('%:p')<cr>

" Копирование
vnoremap <silent> <c-c> y

" Выделение всего текста в документе
nnoremap <silent> <C-a> ggVG

" ----------------- Работа с буферами -------------
"  закрыть текущий буфер
nnoremap <silent> <leader>q :bd<cr>
" Закрыть все буферы
nnoremap <silent> <leader>QA :%bd<cr>
" Закрыть буфер без сохранения изменений 
nnoremap <silent> <leader>Q :bd!<cr>
" На след буфер
nnoremap <silent> <Tab> :bn<cr>
" На предыдущий буфер
nnoremap <silent> <S-Tab> :bp<cr>
vnoremap <silent> <tab> >
vnoremap <silent> <s-tab> <

" Перемещение в режиме вставки
" inoremap <C-h> <Nop>
inoremap <silent> <C-h> <Left>
inoremap <silent> <C-l> <Right>
inoremap <silent> <C-j> <Down>
inoremap <silent> <C-k> <Up>
inoremap <silent> <C-d> <esc>diwa

" Перемещение курсора на строку выше при попытке уйти левее начала строки
nnoremap <silent> <expr> h (col('.') == 1 ? 'k$' : 'h')
vnoremap <silent> <expr> h (col('.') == 1 ? 'k$' : 'h')
imap <silent> <expr> <C-h> (col('.') == 1 ? '<esc>k$a' : '<Left>')

" Перемещение курсора на строку ниже при попытке уйти правее конца строки
nnoremap <silent> <expr> l (col('.')+1 >= col('$') ? 'j0' : 'l')
vnoremap <silent> <expr> l (col('.') >= col('$') ? 'j0' : 'l')
inoremap <silent> <expr> <C-l> (col('.') >= col('$') ? '<esc>j0i' : '<Right>')

" Переназначаем команду j в визуальном режиме
" vnoremap <expr> j (line('.') == line('$')) ? 'V' : 'j'

"Перемещение между окнами
nnoremap <silent> <C-S-H> <C-w>h
nnoremap <silent> <C-S-L> <C-w>l
nnoremap <silent> <C-S-J> <C-w>j
nnoremap <silent> <C-S-K> <C-w>k

" Комментирование кода
nmap <silent> <leader>o gcc
vmap <silent> <leader>o gc


" Вызов which key
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

" Комбинации для fzf.vim
autocmd FileType fzf tnoremap <silent><esc> <c-d>
autocmd FileType fzf tnoremap <silent><C-j> <down>
autocmd FileType fzf tnoremap <silent><C-k> <up>
nnoremap <silent> <leader>ff :Files<cr>
nnoremap <silent> <leader>fw :Rg<cr>
nnoremap <silent> <leader>fb :Buffers<cr>
nnoremap <silent> <leader>fc :Commits<cr>

" Комбинации для Plug
nnoremap <silent> <leader>pi :PlugInstall<cr>
nnoremap <silent> <leader>pc :PlugClean<cr>

" ----------- Комбинации для coc -----------------------
" <Tab> и <Shift-Tab> для навигации по автодополнению
inoremap <silent><expr> <c-j> coc#pum#visible() ? coc#pum#next(1) : "\<down>"
inoremap <silent><expr> <c-k> coc#pum#visible() ? coc#pum#prev(1) : "\<up>"

" Используйте <CR> для подтверждения выбранного элемента автокомплита
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" Переход к определению (Go to Definition)
nmap <silent> gd <Plug>(coc-definition)

" Переход к реализации (Go to Implementation)
nmap <silent> gi <Plug>(coc-implementation)

" Показать все ссылки (Show References)
nmap <silent> gr <Plug>(coc-references)

" Показать документацию для текущего символа (Show Documentation)
" nnoremap <silent> K :call ShowDocumentation()<CR>
nnoremap <silent> K :call CocActionAsync('doHover')<CR>

nnoremap <leader>d :call CocAction('diagnosticInfo')<CR>

" Переименовать переменную/функцию (Rename Symbol)
nmap <silent> <leader>rn <Plug>(coc-rename)

" Применить доступные действия (Apply Code Actions)
nmap <silent> <leader>ca <Plug>(coc-codeaction)


" ---------------------- Комбинации для работы с сессиями -------------------------------------
nnoremap <silent> <leader>ss :SSave<CR>
nnoremap <silent> <leader>sl :SLoad<cr>
nnoremap <silent> <leader>sd :SDelete<cr>
nnoremap <silent> <leader>sc :SCloce<cr>
nnoremap <silent> <leader>st :Startify<cr>


" ---------------------- Комбинации для работы с git ----------------
nnoremap <silent> <leader>gs :Git status<cr>
nnoremap <silent> <leader>gf :Git pull 
nnoremap <silent> <leader>gp :Git push 
nnoremap <silent> <leader>gc :Git commit -m ""<left>
nnoremap <silent> <leader>ga :Git add -A<cr>
nnoremap <silent> <leader>gb :Git branch 
nnoremap <silent> <leader>gw :Git switch 

" ---------------------- Комбинации для codeium ---------------------
imap <Tab> <tab>
imap <script><silent><nowait><expr> <c-i> codeium#Accept()
" imap <script><silent><nowait><expr> <C-h> codeium#AcceptNextWord()
" imap <script><silent><nowait><expr> <C-j> codeium#AcceptNextLine()
" imap <C-;>   <Cmd>call codeium#CycleCompletions(1)<CR>
" imap <C-,>   <Cmd>call codeium#CycleCompletions(-1)<CR>
imap <C-x>   <Cmd>call codeium#Clear()<CR>

" -------------------- Комбинации для работы с терминалом ----------------
" Нормальный режим в терминале
tnoremap <silent> <C-x> <C-\><C-N>

" Перемещение курсора в режиме терминала
tnoremap <silent> <C-h> <Left>
tnoremap <silent> <C-l> <Right>
tnoremap <silent> <C-j> <Down>
tnoremap <silent> <C-k> <Up>

" floaterm
autocmd FileType floaterm tnoremap <silent><esc> <C-\><C-n>:FloatermToggle<CR>
nnoremap <silent> <leader>t :FloatermToggle<cr>
tnoremap <silent> <c-t> <C-\><C-n>:FloatermNew<CR>
tnoremap <silent> <c-n> <C-\><C-n>:FloatermNext<CR>
tnoremap <silent> <c-w> <C-\><C-n>:FloatermKill<CR>

" ------------------ Настройка файлового менеджера -----------------------
" Работа с файловой системой NerdTree
" nnoremap <leader>e :NERDTreeToggle<CR>
" autocmd FileType nerdtree nmap <buffer> O go
" autocmd FileType nerdtree nmap <buffer> <esc> q
" autocmd FileType nerdtree nmap <buffer> . C
" autocmd FileType nerdtree nmap <buffer> a ma
" autocmd FileType nerdtree nmap <buffer> d md
" autocmd FileType nerdtree nmap <buffer> <BS> u

nnoremap <silent> <leader>e :CocCommand explorer
    \ --sources=file+
    \ --quit-on-open
    \ --open-action-strategy=previousBuffer
    \<CR>
autocmd FileType coc-explorer nnoremap <silent><buffer> <esc> :bd<CR>

