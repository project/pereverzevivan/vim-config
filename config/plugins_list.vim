call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree' " Файловый менеджер

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " Хуй знает, что это
Plug 'tpope/vim-commentary' " Чтобы комментировать строки целиком
Plug 'liuchengxu/vim-which-key' " Чтобы высвечивались подсказки по горячим клавишам 
Plug 'ryanoasis/vim-devicons' " Добавление иконок
Plug 'mhinz/vim-startify' " Стартовый экран
Plug 'tpope/vim-surround' " Оборачивание выделенного текста в другие символы
Plug 'jiangmiao/auto-pairs' " Автоматическое добавление закрывающей скобки
Plug 'nathanaelkane/vim-indent-guides' " Подсветка отступов
Plug 'mg979/vim-visual-multi' " Мультикурсор


" Поиск файлов
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Строка состояния и список открытых буферов
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Плагины для работы с git
Plug 'tpope/vim-fugitive' " Вызов команд гита внутри вима
Plug 'airblade/vim-gitgutter' " Отображение изменений в левой части строки
Plug 'christoomey/vim-conflicted' " Отображение конфликтов

" Терминал
Plug 'voldikss/vim-floaterm'

" LSP
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Установка плагинов для подсветки синтаксиса
" Plug 'fatih/vim-go'
Plug 'sheerun/vim-polyglot'

" Codeium
Plug 'Exafunction/codeium.vim'

" Цветовые темы
Plug 'catppuccin/vim', { 'as': 'catppuccin' }
Plug 'joshdick/onedark.vim'

call plug#end()

