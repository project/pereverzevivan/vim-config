" Кодировка текста
set encoding=UTF-8

" Проверка орфографии
" set spell
set spelllang=ru_ru
set spelloptions=camel

" Чтобы можно было открыть новый буфер без сохранения старого
set hidden

" Отступы
set autoindent
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set cindent         " Стиль отступов, похожий на C (хорошо работает с `{}` в коде)

"Нумерация строк
set number
set relativenumber

"Поддержка мыши
set mouse=a

"Игнорирование регистра при поиске
set ignorecase
set smartcase

" Правильное отображение цветов
set termguicolors


" Изменение вида курсора в зависимости от режима
if has("nvim") || has("termguicolors")
  let &t_SI = "\e[6 q"  " Режим вставки — курсор-черта
  let &t_SR = "\e[4 q"  " Режим замены — курсор подчеркивание
  let &t_EI = "\e[2 q"  " Остальные режимы — курсор-блок
endif

" Задержка распознания команд между последовательным нажатием нескольких клавиш
set timeoutlen=300

" Убрать знаки тильды в пустых частях буфера
set fillchars=eob:\ 

" Нужно для того, чтобы вим работал с общим буфером обмена
set clipboard=unnamedplus

" Настройка строки состояния и списка открытых вкладок
let g:airline_theme = 'catppuccin_frappe'
let g:airline#extensions#tabline#enabled = 1 " Включить отображение списка вкладок
let g:airline#extensions#tabline#formatter = 'unique_tail' " Формат отображения вкладок
let g:airline#extensions#tabline#show_buffers = 1 " Показать вкладки
let g:airline#extensions#tabline#show_tab_count = 1 " Показывается количество открытых вкладок
let g:airline_powerline_fonts = 1 " Включить поддержку Powerline шрифтов
let g:airline#extensions#keymap#enabled = 1 " Не показывать текущий маппинг
let g:Powerline_symbols = 'unicode' " Поддержка unicode
let g:airline#extensions#tabline#left_sep = '|'
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_detect_spell = 0 " Убираем информацию об используемых словарях
let g:airline#extensions#whitespace#enabled = 0 " Отключение отображения trailing whitespace
let g:airline#extensions#xkblayout#enabled = 0
let g:airline_section_z = '%3p%% %l:%c' " Переопределяем отображение секции z в vim-airline
let g:airline#extensions#branch#enabled = 1 " Отображение текущей ветки репозитория
let g:airline#extensions#term#enabled = 1 " Применение конфигурации к терминалу
let g:airline_section_z_term = '%3p%% %l:%c' " Переопределяем отображение секции z в vim-airline в режиме терминала
let g:airline#extensions#lsp#enabled = 1 " Показывать использующийся в данный момент LSP сервер


" ----------------------  Настройка NerdTree ------------------------------
let g:NERDTreeChDirMode = 2 " Автоматическая смена cwd при выборе корневой папки
let g:NERDTreeQuitOnOpen = 1 " Закрывать NERDTree при открытии файла
let NERDTreeShowHidden=1 " Показывать скрытые файлы


" ----------------- Настройка COC ------------
" Настройки для автокомплита
set completeopt-=preview

" ----------------- Настройка подсветки синтаксиса ---------------------
" Подсветка синтаксиса
syntax on 

" vim-go
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_trailing_whitespace_error = 1

" vim-jsx-pretty
let g:jsx_ext_required = 0  " Поддержка JSX внутри .js файлов
let g:jsx_pretty_highlight_close_tag = 1  " Подсветка закрывающих тэгов


" ---------------- Настройка startify -----------------------------------
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Последние сессии'] },
      \ { 'type': 'commands',  'header': ['   Быстрые действия'] },
      \ { 'type': 'files',     'header': ['   Недавние файлы'] },
      \ { 'type': 'bookmarks', 'header': ['   Закладки'] },
      \ ]

" Настраиваем путь для сессий
let g:startify_session_dir = '~/.vim/sessions'
let g:max_sessions = 7  " Максимальное количество сохраняемых сессий

" Добавляем быстрые команды
let g:startify_commands = [
      \ { 'v': ['Вернуться к последней сессии', ':SLoad last_session.vim'] },
      \ { 'f': ['Поиск файла', ':Files'] },
      \ { 'w': ['Поиск слова', ':Rg'] },
      \ { 'n': ['Открыть файловый менеджер', ':NERDTreeToggle'] },
      \ ]

" Рисунок на стартовом экране
let g:startify_custom_header = [
\  '      ███╗   ███╗███████╗ ██████╗  █████╗     ██╗   ██╗██╗███╗   ███╗    ██╗  ██╗ █████╗ ',
\  '      ████╗ ████║██╔════╝██╔════╝ ██╔══██╗    ██║   ██║██║████╗ ████║    ╚██╗██╔╝██╔══██╗',
\  '      ██╔████╔██║█████╗  ██║  ███╗███████║    ██║   ██║██║██╔████╔██║     ╚███╔╝ ╚█████╔╝',
\  '      ██║╚██╔╝██║██╔══╝  ██║   ██║██╔══██║    ╚██╗ ██╔╝██║██║╚██╔╝██║     ██╔██╗ ██╔══██╗',
\  '      ██║ ╚═╝ ██║███████╗╚██████╔╝██║  ██║     ╚████╔╝ ██║██║ ╚═╝ ██║    ██╔╝ ██╗╚█████╔╝',
\  '      ╚═╝     ╚═╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝      ╚═══╝  ╚═╝╚═╝     ╚═╝    ╚═╝  ╚═╝ ╚════╝ ',
\]

" Имя сессии по умолчанию
let g:last_session = g:startify_session_dir . '/last_session.vim'

" Автоматическое сохранение и перезапись сессии при выходе
autocmd VimLeave * execute 'mksession!' g:last_session


" Функция для сохранения сессии с уникальным именем (Пока что нигде не
" используется, так как проще создавать новые сессии руками и давать им имена
" самому)
function! SaveSessionOnCwdChange()
    " Заменяем `/` на `#` для уникальности имени файла
    let l:session_name = substitute(getcwd(), '/', '_', 'g')
    execute 'mksession!' g:startify_session_dir . '/' . l:session_name . '.vim'
endfunction

function! TrimOldSessions()
    let l:sessions = split(glob(g:startify_session_dir . '/*.vim'), '\n')
    if len(l:sessions) > g:max_sessions
        " Сортируем по времени модификации и удаляем старые сессии
        call sort(l:sessions, {a, b -> getftime(a) - getftime(b)})
        let l:to_delete = l:sessions[0 : len(l:sessions) - g:max_sessions - 1]
        for session in l:to_delete
            call delete(session)
        endfor
    endif
endfunction


" ----------------------- Настройка подсветки отступов -----------------
" Включить подсветку отступов
let g:indent_guides_enable_on_vim_startup = 1

" Отключить автоматическую настройку цветов
let g:indent_guides_auto_colors = 0

" Установить цвета линий
highlight IndentGuidesOdd guibg=#303446 ctermbg=1 " Установить цвет для нечетных направляющих
highlight IndentGuidesEven guibg=#51576d ctermbg=1 " Установить цвет для четных направляющих

" Настройка ширины линий
let g:indent_guides_guide_size = 1

" С какого уровня глубины начинать подсветку
let g:indent_guides_start_level = 2

" ---------- Настройка AutoPairs -----------------
let g:VM_mouse_mappings = 1


" --------------- Настройка codeium ---------------
let g:codeium_enabled = v:true
let g:codeium_disable_bindings = 1
let g:codeium_no_map_tab = v:true

autocmd BufEnter ~/go/src/gitlab.cnii-jest.ru/* let g:codeium_enabled = 0
if expand('%:p') =~ '^/home/ivan/go/src/gitlab.cnii-jest.ru/'
    let g:codeium_enabled = 0
endif

" -------------- Настройка floaterm -----------
let g:floaterm_width = 0.8
let g:floaterm_height = 0.8

" Настройка vim-auto-pairs
let g:AutoPairsMapCh = 0

