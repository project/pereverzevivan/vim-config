" Определяем главную клавишу
let mapleader = " "

" Добавляем возможность использовать русскую раскладку для навигации и прочего
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz

" Основные
nnoremap <leader>ц :w<cr>
nnoremap <c-й> :q<cr>
nnoremap <c-Й> :q!<cr>
nnoremap <leader>г ~
nnoremap <c-я> u
nnoremap <c-ы> :w<cr>
inoremap оо <esc>

" Перемещение курсора на строку выше при попытке уйти левее начала строки
nnoremap <expr> р (col('.') == 1 ? 'k$' : 'h')
vnoremap <expr> р (col('.') == 1 ? 'k$' : 'h')
imap <expr> <C-h> (col('.') == 1 ? '<esc>k$a' : '<Left>')

" Перемещение курсора на строку ниже при попытке уйти правее конца строки
nnoremap <expr> д (col('.')+1 >= col('$') ? 'j0' : 'l')
vnoremap <expr> д (col('.') >= col('$') ? 'j0' : 'l')
inoremap <expr> <C-l> (col('.') >= col('$') ? '<esc>j0i' : '<Right>')


" ----------------- Работа с буферами -------------
"  закрыть текущий буфер
nnoremap <leader>й :bd<cr>
" Закрыть все буферы
nnoremap <leader>ЙФ :%bd<cr>
" Закрыть буфер без сохранения изменений
nnoremap <leader>Й :bd!<cr>


" ------------------ Настройка файлового менеджера -----------------------
nnoremap <leader>у :CocCommand explorer<CR>
